# Gugutoyer Core
## Um simplório bot que twita brinquedos do Gugu.

Versão: 2.0.0 - Sério que você se importaria com isso 2 - O inimigo agora é outro?

*Licença: GPL 3.0 ou superior*

----------

## Como usa esse trem?

Você vai precisar de um banco MySQL/MariaDB, criar um usuário, um schema, promover o grant de EXECUTE ao usuário para esse schema, criar tabela/function/procedures e adicionar as palavras à tabela. Provavelmente todos os .sql necessários estão no repositório, na pasta Database.

É necessária uma intervenção manual para popular a tabela de palavras filtradas. SafeSearch é longe de ser imbatível, e estamos trabalhando com linguagem aqui. Vai que dá ruim. Mexer com uma entrada praticamente imprevisível tem essa desvantagem. A procedure GET_RANDOM_POOL retorna 100 resultados para serem analisados. Bons resultados podem ser inseridos na tabela de filtro.

Você precisa do .Net Core SDK e Runtime 3.0 para compilar o projeto.

Se estiver usando Ubuntu e a distribuição do .Net através de um pacote Snap, recomendo criar um alias:

```bash
snap alias dotnet-sdk.dotnet dotnet
```

Deploy: (Isso é deploy? Faça-me o favor)

```bash
#Clone o repositório
$ git clone https://git.cotti.moe/cotti/GugutoyerCore.git
#Entre na pasta do .csproj
$ cd GugutoyerCore/GugutoyerCore
#Faça o Restore dos pacotes Nuget
$ dotnet restore
#Faça um Publish de Release
$ dotnet publish -c Release
#Uma pasta bin/Release/netcoreapp3.0/publish com as DLLs será criada. Copie o arquivo settings.sample.toml para ela como settings.toml, e preencha as informações necessárias
$ cp settings.sample.toml bin/Release/netcoreapp3.0/publish/settings.toml
$ vim bin/Release/netcoreapp3.0/publish/settings.toml
#Execute. nesse exemplo, o repositório foi clonado dentro da pasta /opt. Um cronjob está disponível na pasta Cron para automatizar o processo. Note que ele assume que a pasta é aquela e que o alias do binário do .Net Core Runtime foi criado
$ /snap/bin/dotnet /opt/GugutoyerCore/GugutoyerCore/bin/Release/netcoreapp3.0/publish/GugutoyerCore.dll
```
---

## Quais são seus planos para o futuro?

- Refatorar algumas coisas
- Automatizar mais alguns processos
- Mais templates