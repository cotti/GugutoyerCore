CREATE 
    ALGORITHM = MERGE 
    SQL SECURITY DEFINER
VIEW `UNUSED_WORDS` AS
    SELECT 
        `F`.`palavra_id` AS `PALAVRA_ID`, `P`.`palavra` AS `PALAVRA`
    FROM
        `filtro` `F`
        JOIN `palavras` `P` on `P`.`palavra_id` = `F`.`palavra_id`
    WHERE
        (`P`.`usado` = 0)