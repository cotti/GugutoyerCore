﻿using GugutoyerCore.ImageProcessing;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Interaction
{
    class TestInteractionBroker : IInteractionBroker
    {
        private byte[] media;
        private readonly string word;
        private readonly IImageProcessor imageProcessor;
        private readonly IDictionary<string, string> config;
        public TestInteractionBroker(IDictionary<string, string> config, IImageProcessor imageProcessor, string word)
        {
            this.config = config;
            this.imageProcessor = imageProcessor;
            this.word = word;
        }
        public void Prepare() { }

        public bool SendMedia()
        {
            try
            {
                media = imageProcessor.CreateImage();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public bool SendStatus()
        {
            try
            {
                using IMagickImage res = new MagickImage(media);
                res.Write($"result_{word}.jpg");
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool SendWarningMessage(int remaining)
        {
            return true;
        }
    }
}
