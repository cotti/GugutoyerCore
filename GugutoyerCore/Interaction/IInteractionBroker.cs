﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Interaction
{
    public interface IInteractionBroker
    {
        public void Prepare();
        public bool SendMedia();
        public bool SendStatus();
        public bool SendWarningMessage(int remaining);
    }
}
