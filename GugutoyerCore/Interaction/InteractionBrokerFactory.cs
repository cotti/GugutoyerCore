﻿using GugutoyerCore.ImageProcessing;
using System.Collections.Generic;

namespace GugutoyerCore.Interaction
{
    class InteractionBrokerFactory : IInteractionBrokerFactory
    {
        public IInteractionBroker GetBroker(InteractionBrokerType type, IDictionary<string, string> config, IImageProcessor imageProcessor, string word)
        {
            return type switch
            {
                InteractionBrokerType.Twitter => new TwitterInteractionBroker(config, imageProcessor, word),
                InteractionBrokerType.Test => new TestInteractionBroker(config, imageProcessor, word),
                _ => new TestInteractionBroker(config, imageProcessor, word)
            };
        }
    }
}
