﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using CoreTweet;
using GugutoyerCore.ImageProcessing;

namespace GugutoyerCore.Interaction
{
    class TwitterInteractionBroker : IInteractionBroker
    {
        private MediaUploadResult uploadResult;
        private Tokens token;
        private byte[] image;

        private readonly IDictionary<string, string> config;
        private readonly string word;
        private readonly IImageProcessor imageProcessor;
        public TwitterInteractionBroker(IDictionary<string,string> config, IImageProcessor imageProcessor, string word)
        {
            this.config = config;
            this.imageProcessor = imageProcessor;
            this.word = word;
        }
        public void Prepare()
        {
            token = Tokens.Create(config["apikey"], config["apisecret"], config["accesstoken"], config["accesstokensecret"]);
        }

        public bool SendMedia()
        {
            try
            {
                image = imageProcessor.CreateImage();
                uploadResult = token.Media.Upload(media: image.AsEnumerable());
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception trying to send media:\n{e.Message}\nStack Trace:\n{e.StackTrace}");
                return false;
            }
            if (uploadResult.MediaId == 0)
                return false;
            return true;
        }

        private bool VerifySpecialEdition()
        {
            var argSpan = Environment.GetCommandLineArgs().AsSpan<string>();
            int idx = argSpan.IndexOf<string>("-s") != -1 ? argSpan.IndexOf<string>("-s") : argSpan.IndexOf<string>("--special");
            return ++idx != 0;
        }

        public bool SendStatus()
        {
            try
            {
                StringBuilder expression = new StringBuilder();
                if (VerifySpecialEdition())
                    expression.Append("Edição de Colecionador:\n\n");
                expression.Append(word.First().ToString(CultureInfo.InvariantCulture).ToUpper(CultureInfo.InvariantCulture) + word.Substring(1) + " do Gugu");
                Console.WriteLine($"Trying to send: {expression.ToString()}. Media upload result JSON: {uploadResult.Json}\n\nMedia upload id: {uploadResult.MediaId}");
                token.Statuses.Update(status: expression.ToString(), media_ids: new long[] { uploadResult.MediaId });
            }
            catch(System.Net.WebException we)
            {
                Console.WriteLine($"WebException caught: {we.Response}\n{we.Message}\n{we.Status}\n{we.Data}\n{we.TargetSite}\n{we.StackTrace}");
            }
            catch(Exception e)
            {
                Console.WriteLine($"Exception trying to send media:\n{e.Message}\nStack Trace:\n{e.StackTrace}");
                if (!(e.InnerException is null))
                    Console.WriteLine($"Inner Exception :\n{e.InnerException.Message}\nStack Trace:\n{e.InnerException.StackTrace}");
                return false;
            }
            return true;
        }

        public bool SendWarningMessage(int remaining)
        {
            if(remaining < 100 && (remaining % 5) == 0)
            {
                try
                {
                    var message = $"As mensagens estão começando a ficar escassas, tem tipo {remaining} agora. Faça uma filtragem.";
                    token.DirectMessages.Events.New(message, long.Parse(config["warninghandlerid"], CultureInfo.InvariantCulture));
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception trying to send media:\n{e.Message}\nStack Trace:\n{e.StackTrace}");
                    if (!(e.InnerException is null))
                        Console.WriteLine($"Inner Exception :\n{e.InnerException.Message}\nStack Trace:\n{e.InnerException.StackTrace}");
                    return false;
                }
            }
            return true;
        }
    }
}
