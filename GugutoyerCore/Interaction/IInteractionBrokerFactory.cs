﻿using GugutoyerCore.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Interaction
{
    public enum InteractionBrokerType
    {
        Twitter,
        Test
    }
    public interface IInteractionBrokerFactory
    {
        public IInteractionBroker GetBroker(InteractionBrokerType type, IDictionary<string, string> config, IImageProcessor imageProcessor, string word);
    }
}
