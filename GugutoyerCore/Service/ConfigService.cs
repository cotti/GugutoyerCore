﻿using Nett;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using GugutoyerCore.Templates;
using System.Linq;

namespace GugutoyerCore.Service
{
    class ConfigService
    {
        private static Dictionary<string, Dictionary<string,string>> _config = null;

        public static Dictionary<string, Dictionary<string,string>> Config
        {
            get
            {
                if (_config is null)
                {
                    LoadConfig();
                }
                return _config;
            }
        }

        private static void LoadConfig()
        {
            _config = Toml.ReadFile(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "/settings.toml").Get<Dictionary<string, Dictionary<string,string>>>();
        }

        private static List<Template> _templates = null;
        public static List<object> Templates
        {
            get
            {
                if (_templates is null)
                {
                    LoadTemplates();
                }
                return _templates.Cast<object>().ToList();
            }
        }

        private static void LoadTemplates()
        {
            string reader = File.ReadAllText(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "/templates.json");
            _templates = JsonSerializer.Deserialize<List<Template>>(reader);
        }


    }
}
