﻿using GugutoyerCore.Database;
using System.Linq;
using System.Text;
using GugutoyerCore.Service;
using CoreTweet;
using GugutoyerCore.ImageProcessing;
using GugutoyerCore.Interaction;
using System.Globalization;
using System;

namespace GugutoyerCore
{
    class Program
    {
        static void Main()
        {
            using IGugutoyerContext gugu = new GugutoyerContextFactory().GetContext((Environment.GetCommandLineArgs().Length > 2) ? GugutoyerContextType.SpecialEdition : GugutoyerContextType.DB);
            int remaining = gugu.GetRemaining();
            
            if (remaining > 0)
                RunBot(gugu, remaining);
        }

        static void RunBot(IGugutoyerContext gugu, int remaining)
        {
            int next = gugu.GetNext();
            string word = gugu.GetNextWord(next);
            IImageProcessor imageProcessor = new ImageProcessorFactory().GetProcessor(ImageProcessorType.Web, ConfigService.Config["google"], ConfigService.Templates, word);
            IInteractionBroker broker = new InteractionBrokerFactory().GetBroker(InteractionBrokerType.Twitter, ConfigService.Config["twitter"], imageProcessor, word);

            broker.Prepare();
            broker.SendMedia();
            broker.SendStatus();
            broker.SendWarningMessage(remaining);
        }
    }
}
