﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Não capturar exceptions de tipos genéricos", Justification = "<Pendente>", Scope = "member", Target = "~M:GugutoyerCore.Interaction.TestInteractionBroker.SendStatus~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Não capturar exceptions de tipos genéricos", Justification = "<Pendente>", Scope = "member", Target = "~M:GugutoyerCore.Interaction.TwitterInteractionBroker.SendMedia~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Não capturar exceptions de tipos genéricos", Justification = "<Pendente>", Scope = "member", Target = "~M:GugutoyerCore.Interaction.TwitterInteractionBroker.SendStatus~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Não capturar exceptions de tipos genéricos", Justification = "<Pendente>", Scope = "member", Target = "~M:GugutoyerCore.Interaction.TestInteractionBroker.SendMedia~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Não capturar exceptions de tipos genéricos", Justification = "<Pendente>", Scope = "member", Target = "~M:GugutoyerCore.ImageProcessing.WebImageProcessor.SafeGetImage(System.Collections.Generic.IEnumerator{GoogleApi.Entities.Search.Common.Item})~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:As propriedades de coleção devem ser somente leitura", Justification = "<Pendente>", Scope = "member", Target = "~P:GugutoyerCore.Templates.Template.Effects")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:As propriedades de coleção devem ser somente leitura", Justification = "<Pendente>", Scope = "member", Target = "~P:GugutoyerCore.Templates.Effect.Values")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Não capturar exceptions de tipos genéricos", Justification = "<Pendente>", Scope = "member", Target = "~M:GugutoyerCore.Interaction.TwitterInteractionBroker.SendWarningMessage(System.Int32)~System.Boolean")]
