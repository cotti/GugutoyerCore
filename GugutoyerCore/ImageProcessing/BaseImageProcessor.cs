﻿using GugutoyerCore.Templates;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace GugutoyerCore.ImageProcessing
{
    abstract class BaseImageProcessor : IImageProcessor
    {
        protected string word;
        protected readonly IDictionary<string, string> config;
        protected IList<Template> templates;
        protected Template activeTemplate;

        protected BaseImageProcessor(IDictionary<string, string> config, IList<object> templates, string word)
        {
            this.templates = templates.Cast<Template>().ToList();
            this.config = config;
            this.word = word;
        }

        protected void SelectTemplate()
        {
            activeTemplate = templates[new Random().Next(templates.Count)];
        }
        protected void PrepareSource(MagickImage imgsource, MagickImage imgsourceEffect, int templateWidth, int templateHeight)
        {
            //Background effect prep. 
            //Crop creates the base for the blurred background to fill the void
            imgsourceEffect.Crop(new MagickGeometry()
            {
                Less = true,
                Greater = false,
                IgnoreAspectRatio = false,
                FillArea = true,
                Width = (imgsourceEffect.Width > templateWidth) ? templateWidth : imgsourceEffect.Width,
                Height = (imgsourceEffect.Height > templateHeight) ? templateHeight : imgsourceEffect.Height,
            }, Gravity.Center);

            //Blur the image.
            imgsourceEffect.Blur(10, 10);

            //Fill the whole possible extension
            imgsourceEffect.Scale(new MagickGeometry()
            {
                Less = true,
                Greater = false,
                IgnoreAspectRatio = false,
                FillArea = true,
                Width = templateWidth,
                Height = templateHeight
            });

            //RePage() is required at this point.
            imgsourceEffect.RePage();

            //Prep the actual image.
            //Scale it to touch the vertical extremities. Let the blurred background take care of the horizontal space.
            imgsource.Scale(new MagickGeometry()
            {
                Greater = (imgsource.Width > templateWidth || imgsource.Height > templateHeight ? true : false),
                Less = (imgsource.Width <= templateWidth && imgsource.Height <= templateHeight ? true : false),
                //Greater = true,
                //Less = true,
                Height = templateHeight,
                IgnoreAspectRatio = false,
                FillArea = true,
            });

            //RePage()
            imgsource.RePage();

            //Cut any extra borders in the image. --Is this needed at this point? hmm...
            imgsource.Shave(
                (imgsource.Width > imgsourceEffect.Width) ? ((imgsource.Width - imgsourceEffect.Width) / 2) : 0,
                (imgsource.Height > imgsourceEffect.Height) ? ((imgsource.Height - imgsourceEffect.Height) / 2) : 0);

            //Glue the source above the blurred background.
            imgsourceEffect.Composite(imgsource, Gravity.Center, CompositeOperator.SrcOver);
            
            //Readapt the page and shave again. --Is this needed yet another time? hmm...
            imgsourceEffect.Page = new MagickGeometry()
            {
                X = (templateWidth - imgsourceEffect.Width > 0) ? ((templateWidth - imgsourceEffect.Width) / 2) : 0,
                Y = (templateHeight - imgsourceEffect.Height > 0) ? ((templateHeight - imgsourceEffect.Height) / 2) : 0
            };

            imgsourceEffect.Shave(
                (imgsourceEffect.Width > templateWidth) ? ((imgsourceEffect.Width - templateWidth) / 2) : 0,
                (imgsourceEffect.Height > templateHeight) ? ((imgsourceEffect.Height - templateHeight) / 2) : 0);
        }
        protected void ApplyEffects(MagickImage image, string word)
        {
            if (!(activeTemplate is null) && !(image is null))
                foreach (Effect effect in activeTemplate.Effects)
                {
                    switch (effect.Type)
                    {
                        case "draw":
                            {
                                var settings = new MagickReadSettings()
                                {
                                    BackgroundColor = MagickColors.Transparent,
                                    FontFamily = effect.Values[0].GetString(),
                                    StrokeColor = new MagickColor(effect.Values[2].GetString()),
                                    FillColor = new MagickColor(effect.Values[3].GetString()),
                                    BorderColor = new MagickColor(effect.Values[4].GetString()),
                                    StrokeWidth = effect.Values[5].GetDouble(),
                                    TextGravity = (Gravity)effect.Values[9].GetInt32(),
                                    Width = image.Width - activeTemplate.HorizontalCut,
                                    Height = (effect.Values[11].GetBoolean() ? image.Height - activeTemplate.VerticalCut : 100),
                                    Page = new MagickGeometry(image.Width, image.Height),
                                    Defines = new ImageMagick.Formats.CaptionReadDefines()
                                    {
                                        MaxFontPointsize = effect.Values[1].GetInt32()
                                    },
                                    TextKerning = effect.Values[10].GetDouble()
                                };
                                //Caption does the resizing text magic.
                                using var textImage = new MagickImage((effect.Values[11].GetBoolean() ? "caption: " : "label: ") + word[0].ToString(CultureInfo.InvariantCulture).ToUpper(CultureInfo.InvariantCulture) + word.Substring(1) + (effect.Values[8].GetBoolean() ? " do Gugu" : ""), settings);

                                image.Composite(textImage, (Gravity)effect.Values[9].GetInt32(), effect.Values[6].GetInt32(), effect.Values[7].GetInt32(), CompositeOperator.SrcOver);
                            }
                            break;
                        case "distort":
                            var enumerator = effect.Values[1].EnumerateArray();
                            enumerator.MoveNext();
                            //Left-Up, Left-Down, Right-Up, Right-Down
                            //Initial X, Initial Y, Final X, Final Y 
                            double[] enumvalue = new double[effect.Values[1].GetArrayLength()];
                            for (int i = 0; i < effect.Values[1].GetArrayLength(); ++i, enumerator.MoveNext())
                                enumvalue[i] = enumerator.Current.GetDouble();
                            image.Distort((DistortMethod)effect.Values[0].GetInt32(), enumvalue);
                            break;
                        default:
                            break;
                    }
                }
        }
        public abstract byte[] CreateImage();
    }
}
