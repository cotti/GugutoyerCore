﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.ImageProcessing
{
    class ImageProcessorFactory : IImageProcessorFactory
    {
        public IImageProcessor GetProcessor(ImageProcessorType type, IDictionary<string, string> config, IList<object> templates, string word)
        {
            return type switch
            {
            ImageProcessorType.Web => new WebImageProcessor(config, templates, word),
            ImageProcessorType.Test => new TestImageProcessor(config, templates, word),
            _ => new TestImageProcessor(config, templates, word)
            };
        }
    }
}
