﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.ImageProcessing
{
    class TestImageProcessor : BaseImageProcessor
    {
        public TestImageProcessor(IDictionary<string, string> config, IList<object> templates, string word) : base(config, templates, word) { }
        public override byte[] CreateImage()
        {
            MagickNET.SetTempDirectory(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));
            //Testing purposes:
            using MagickImage imgsource = new MagickImage(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + $"/{word}_original.png");
            using MagickImage imgsourceEffect = new MagickImage(imgsource.Clone());

            using MagickImageCollection imageCollection = new MagickImageCollection();
            using MagickImageCollection sourceEffectCollection = new MagickImageCollection();

            SelectTemplate();

            using MagickImage imgtemplate = new MagickImage(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + activeTemplate.File,
                new MagickReadSettings()
                {
                    BackgroundColor = MagickColors.Transparent
                });

            //Prepare the source material
            PrepareSource(imgsource, imgsourceEffect, imgtemplate.Width, imgtemplate.Height);

            //Apply effects for the template
            ApplyEffects(imgsourceEffect, word);

            imageCollection.Add(imgsourceEffect);

            imageCollection.Add(imgtemplate);

            using MagickImage res = new MagickImage(imageCollection.Merge());
            res.Page = new MagickGeometry()
            {
                Width = imgtemplate.Width,
                Height = imgtemplate.Height,
            };
            res.Transparent(MagickColors.Transparent);
            return res.ToByteArray(MagickFormat.Jpg);
        }
    }
}
