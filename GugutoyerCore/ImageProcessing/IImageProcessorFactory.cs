﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.ImageProcessing
{
    public enum ImageProcessorType
    {
        Web,
        Test
    }
    public interface IImageProcessorFactory
    {
        public IImageProcessor GetProcessor(ImageProcessorType type, IDictionary<string, string> config, IList<object> templates, string word);
    }
}
