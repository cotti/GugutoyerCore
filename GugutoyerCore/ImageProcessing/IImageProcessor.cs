﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.ImageProcessing
{
    public interface IImageProcessor
    {
        public byte[] CreateImage();
    }
}
