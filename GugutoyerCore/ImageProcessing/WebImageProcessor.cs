﻿using System;
using System.Collections.Generic;
using System.Text;
using GoogleApi.Entities.Search.Image.Request;
using GoogleApi.Entities.Search.Common;
using System.Net;
using System.Threading.Tasks;
using ImageMagick;
using System.Globalization;

namespace GugutoyerCore.ImageProcessing
{
    class WebImageProcessor : BaseImageProcessor
    {
        //Will be populated by the Image Search API
        private byte[] rawSourceImg;
        private bool stopTrying;

        public WebImageProcessor(IDictionary<string, string> config, IList<object> templates, string word) : base(config, templates, word) { }
        public override byte[] CreateImage()
        {
            if (String.IsNullOrEmpty(word))
                return null;
            Console.WriteLine("Creating data for word: " + word);
            //Acquire image from Google Images
            if (!GetFile(word))
                return null;

            MagickNET.SetTempDirectory(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));

            using MagickImage imgsource = new MagickImage(rawSourceImg);
            using MagickImage imgsourceEffect = new MagickImage(imgsource.Clone());

            using MagickImageCollection imageCollection = new MagickImageCollection();
            using MagickImageCollection sourceEffectCollection = new MagickImageCollection();

            SelectTemplate();

            using MagickImage imgtemplate = new MagickImage(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + activeTemplate.File,
                new MagickReadSettings()
                {
                    BackgroundColor = MagickColors.Transparent
                });
            //Prepare the source material
            PrepareSource(imgsource, imgsourceEffect, imgtemplate.Width, imgtemplate.Height);

            //Apply effects for the template
            ApplyEffects(imgsourceEffect, word);

            imageCollection.Add(imgsourceEffect);

            imageCollection.Add(imgtemplate);

            using MagickImage res = new MagickImage(imageCollection.Merge());
            res.Page = new MagickGeometry()
            {
                Width = imgtemplate.Width,
                Height = imgtemplate.Height,
            };
            res.Transparent(MagickColors.Transparent);
            return res.ToByteArray(MagickFormat.Jpg);
        }

        private int GetIndex()
        {
            var argSpan = Environment.GetCommandLineArgs().AsSpan<string>();
            int idx = argSpan.IndexOf<string>("-i") != -1 ? argSpan.IndexOf<string>("-i") : argSpan.IndexOf<string>("--index");
            return ++idx;
        }

        private bool GetFile(string word)
        {
            int start; 
            if (!int.TryParse(Environment.GetCommandLineArgs()[GetIndex()], out start))
                start = 0;
            var request = new ImageSearchRequest
            {
                Key = config["apikey"],
                SearchEngineId = config["searchengineid"],
                ImageOptions = new SearchImageOptions()
                {
                    ImageType = GoogleApi.Entities.Search.Common.Enums.ImageType.Photo
                },
                Query = word,
                Options = new SearchOptions()
                {
                    StartIndex = start,
                    SafetyLevel = GoogleApi.Entities.Search.Common.Enums.SafetyLevel.High
                }
            };

            var responseEnumerator = GoogleApi.GoogleSearch.ImageSearch.QueryAsync(request).Result.Items.GetEnumerator();

            while (!SafeGetImage(responseEnumerator) && !stopTrying) ;

            return !(rawSourceImg is null) ? true : false;
        }

        private bool SafeGetImage(IEnumerator<Item> items)
        {
            //If the iterator can't continue, we can't proceed
            if (!items.MoveNext())
            {
                stopTrying = true;
                return false;
            }
            using WebClient client = new WebClient();

            string sourceLink = items.Current.Link;
            try
            {
                using Task<byte[]> t = new Task<byte[]>(() => 
                {
                    try
                    {
                        return client.DownloadData(sourceLink);
                    }
                    catch
                    {
                        return null;
                    }
                });
                TimeSpan ts = TimeSpan.FromSeconds(30);
                t.Start();
                if (!t.Wait(ts) || t.Result == null)
                    return false;
                rawSourceImg = t.Result;
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                return false;
            }
            catch (AggregateException ae)
            {
                foreach (var e in ae.InnerExceptions)
                    Console.WriteLine(e.Message + "\n" + e.StackTrace);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                return false;
            }
            return true;
        }


    }
}
