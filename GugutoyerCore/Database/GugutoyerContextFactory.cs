﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Database
{
    class GugutoyerContextFactory : IGugutoyerContextFactory
    {
        public IGugutoyerContext GetContext(GugutoyerContextType type)
        {
            return type switch
            {
                GugutoyerContextType.DB => new GugutoyerContext(),
                GugutoyerContextType.Test => new TestGugutoyerContext(),
                GugutoyerContextType.SpecialEdition => new SpecialEditionGugutoyerContext(),
                _ => null
            };
        }
    }
}
