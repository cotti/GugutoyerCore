﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Database
{
    public enum GugutoyerContextType
    {
        DB,
        Test,
        SpecialEdition
    }
    public interface IGugutoyerContextFactory
    {
        public IGugutoyerContext GetContext(GugutoyerContextType type);
    }
}
