﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Database
{
    public interface IGugutoyerContext: IDisposable
    {
        public int GetNext();
        public string GetNextWord(int index);

        public int GetRemaining();
    }
}
