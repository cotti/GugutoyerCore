﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Database
{
    class SpecialEditionGugutoyerContext : IGugutoyerContext
    {

        public int GetNext()
        {
            var argSpan = Environment.GetCommandLineArgs().AsSpan<string>();
            int idx = argSpan.IndexOf<string>("-s") != -1 ? argSpan.IndexOf<string>("-s") : argSpan.IndexOf<string>("--special");
            return ++idx;
        }

        public string GetNextWord(int index)
        {
            if (index > 1)
                return Environment.GetCommandLineArgs()[index];
            return string.Empty;
        }

        public int GetRemaining()
        {
            return 1;
        }

        bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
            }
            disposed = true;
        }
    }
}
