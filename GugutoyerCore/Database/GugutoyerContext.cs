﻿using GugutoyerCore.Service;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.Common;

namespace GugutoyerCore.Database
{
    class GugutoyerContext : DbContext, IGugutoyerContext
    {
        private DbConnection _conn;
        private DbConnection Connection
        {
            get
            {
                if (_conn is null)
                    _conn = Database.GetDbConnection();
                if (!(_conn.State == ConnectionState.Open))
                    _conn.Open();
                return _conn;
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conf = ConfigService.Config;
            optionsBuilder.UseMySQL($"Server={conf["db"]["server"]};User Id={conf["db"]["uid"]};Password={conf["db"]["password"]};Database={conf["db"]["schema"]}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { }

        public int GetNext()
        {
            using var command = Connection.CreateCommand();
            command.CommandText = "NEXT_ID";
            command.CommandType = CommandType.StoredProcedure;
            command.Prepare();

            DbParameter result = new MySqlParameter
            {
                Direction = ParameterDirection.ReturnValue
            };

            command.Parameters.Add(result);
            command.ExecuteNonQuery();

            return (int)result.Value;
        }

        public string GetNextWord(int index)
        {
            using var command = Connection.CreateCommand();
            command.CommandText = "GET_NEXT";
            command.CommandType = CommandType.StoredProcedure;
            command.Prepare();

            DbParameter idx = new MySqlParameter("IN_PALAVRA_ID", MySqlDbType.Int32, 10)
            {
                Direction = ParameterDirection.Input,
                Value = index
            };

            DbParameter outres = new MySqlParameter("OUT_PALAVRA", MySqlDbType.VarChar, 100)
            {
                Direction = ParameterDirection.Output
            };

            command.Parameters.Add(idx);
            command.Parameters.Add(outres);
            command.ExecuteNonQuery();

            return (string)outres.Value;
        }

        public int GetRemaining()
        {
            using var command = Connection.CreateCommand();
            command.CommandText = "COUNT_REMAINING";
            command.CommandType = CommandType.StoredProcedure;
            command.Prepare();

            DbParameter result = new MySqlParameter
            {
                Direction = ParameterDirection.ReturnValue
            };

            command.Parameters.Add(result);
            command.ExecuteNonQuery();

            return (int)result.Value;
        }

        bool disposed = false;

        public override void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                _conn.Close();
                _conn = null;
            }
            disposed = true;
            base.Dispose();
        }
    }
}
