﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GugutoyerCore.Database
{
    class TestGugutoyerContext : IGugutoyerContext
    {
        public int GetNext()
        {
            return 23;
        }

        public string GetNextWord(int index)
        {
            if (index == 23)
                return "Doctiloquente";
            return "talismã";
        }

        public int GetRemaining()
        {
            return 10;
        }

        bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
            }
            disposed = true;
        }
    }
}
