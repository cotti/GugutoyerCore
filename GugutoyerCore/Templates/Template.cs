﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GugutoyerCore.Templates
{
    public class Template
    {
        [JsonPropertyName("file")]
        public string File { get; set; }
        [JsonPropertyName("horizontalcut")]
        public int HorizontalCut { get; set; }
        [JsonPropertyName("verticalcut")]
        public int VerticalCut { get; set; }
        [JsonPropertyName("effects")]
        public List<Effect> Effects { get; set; }
    }

    public class Effect
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }
        [JsonPropertyName("values")]
        public List<JsonElement> Values { get; set; }
    }
}
